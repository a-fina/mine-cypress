# 1 Create npm project
npm init

# 2 Prepare GIT and Gitlab project
git init
git add .
git commit -m "First import"
git remote add origin https://gitlab.com/a-fina/mine-cypress.git
git push --set-upstream origin master

# 3 Install Cypress
https://docs.cypress.io/guides/getting-started/installing-cypress.html#Installing
npm install cypress --save-dev

$(npm bin)/cypress open

Add Cypress commands to the scripts field in your package.json file:
npm run cypress:open

# 4 Write first test
https://docs.cypress.io/guides/continuous-integration/introduction.html#Boot-your-server
MineSQL Login

npm run cy:run

# 5 Continuous Integration
https://docs.cypress.io/guides/continuous-integration/introduction.html#Setting-up-CI

# 6 Parallel Test
https://docs.cypress.io/guides/continuous-integration/gitlab-ci.html#Worker-Jobs


# 7 All Test Passed demo.minesql.com
16/03/2021 