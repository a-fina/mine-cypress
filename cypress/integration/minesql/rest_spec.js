describe("The Home Page Login", () => {

  it("successfully create user", () => {
      cy.request({
        method: 'POST', 
        url: '/autogrid/auto-table.jsp',
        form: true,
        body: {
          tableName:'User',
          crudOperation:'create',
          idQuery:undefined,
          context:undefined,
          hidden_columns:undefined,
          entityName:'User',
          FIRST_NAME:'rossi12',
          LAST_NAME:'gin23',
          Email:'gino12.rossi@mailinator.com'
        }
      })
    .then((response) => {
      expect(response.body).to.contains("Aggiornamento effettuato")
    })
  })

  it("login rest jersey", () => {

    cy.request({
      method: 'POST',
      url: '/rest/gui/login/', // baseUrl is prepended to url
      form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
      body: {
        USERNAME: 'admin',
        PASSWORD: 'admin@admin'
      }
    })
    .then((response) => {
      // response.body is automatically serialized into JSON
      expect(response.body).to.have.property('success', true) // true
    })

  })
})
