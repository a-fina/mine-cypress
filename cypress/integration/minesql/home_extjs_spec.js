const name = "name" + Cypress._.random(0, 10000)
const surname = "surname" + Cypress._.random(0, 10000)

describe("The Home Page Login", () => {
  it("successfully login", () => {
    cy.visit("/");

    // Get an input, type into it and verify that the value has been updated
    cy.get("#USERNAME").type("admin").should("have.value", "admin");
    cy.get("#PASSWORD").type("admin@admin").should("have.value", "admin@admin");
    cy.get('[class="x-panel-body x-form"]')
    cy.get('[type="button"]').click();
    cy.get('[class=" x-panel x-border-panel"]'); 
    cy.contains("Group: administrator");
  });

  it("open menu new user", () => {
    cy.get('[class="x-panel-tbar x-panel-tbar-noheader"]')
      .get('[class="x-toolbar-left-row"]')
      .get('[id="ext-gen23"]').click();

    cy.get('[id="ext-gen57"]').trigger('mouseover')
    cy.get('[id="g-shell-4"]').click();

  })

  it("create new user", () => {

    cy.get('[name="FIRST_NAME"]').type(name)
    cy.get('[name="LAST_NAME"]').type(surname)
    cy.get('[name="Email"]').type(name+"."+surname+"@mailinator.com")

    cy.get('[id="save-button"]').find('button').click()

  })

  it("open user table", () => {
    cy.contains("Database").click()
    cy.get('[id="x-menu-el-databaseMenusubMenu1-1"]').trigger('mouseover')
    cy.get('[id="x-menu-el-databaseMenu-sub-6##PUBLIC##USER##localhost"]').click();
  })

  it("check user exists", () => {
    cy.get('[class=" x-window x-window-plain x-resizable-pinned"]')
      .contains(surname)
  })
});
