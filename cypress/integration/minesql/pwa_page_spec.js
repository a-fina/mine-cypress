describe("The PWA Login", () => {
  it("successfully login", () => {
    cy.visit("/login.html");

    // Get an input, type into it and verify that the value has been updated
    cy.get("#card").type("admin").should("have.value", "admin");
    cy.get("#password").type("admin@admin").should("have.value", "admin@admin");

    cy.get('.login100-form-btn').click();

    cy.contains("Dati personali");

  });
});
