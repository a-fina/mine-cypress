/// <reference types="cypress" />

export class GoogleSearch {
    open() {
        cy.visit('https://www.google.com/',{timeout: 6000});
        //cy.contains("Accetto").click();
    }
   
    search(text) {
        cy.get('[action="/search"]').find('input').first().type(text + "{enter}")
        cy.get('#rcnt').should("exist")
    }
}