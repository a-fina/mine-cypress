describe("Google Search and Open First Result", () => {

  it("open google search", () => {
    cy.visit("https://www.google.com", {timeout: 12000});
    //cy.contains("Accetto").click();
    cy.get('[action="/search"]').find('input').first().type("Cypress.io{enter}")
  });

  it("click first result", () => {
    cy.get('[id="rcnt"]')
      .get('[class="g"]').first().find("a").first().click({ multiple: false});
    //cy.contains("Accept Cookies").click();
  });

  it("validate page content", () => {
    cy.get('[data-cy=tag-line] > div')
      .should("contain", "The web has evolved.");
  });

});
