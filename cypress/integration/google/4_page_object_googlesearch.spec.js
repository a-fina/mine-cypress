import {GoogleSearch} from './google-search';

describe("Google Search and Open First Result", () => {
  const googleSearch = new GoogleSearch();

  beforeEach(() => {
    googleSearch.open();
  })

  it('google search for "cypress"', () => {
    googleSearch.search("cypress");
  });

  it('google search for "test automation"', () => {
    googleSearch.search("test automation");
  });

});
