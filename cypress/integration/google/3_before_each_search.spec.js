describe("Google Search and Open First Result", () => {
  beforeEach(() => {
    cy.visit("https://www.google.com");
    //cy.contains("Accetto").click();
  })

  it('google search for "cypress"', () => {
    cy.get('[action="/search"]').find('input').first().type("cypress{enter}")
    cy.get('#rcnt').should("exist")
  });

  it('google search for "test automation"', () => {
    cy.get('[action="/search"]').find('input').first().type("test automation{enter}")
    cy.get('#rcnt').should("exist")
  });

});
