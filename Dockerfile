FROM cypress/browsers:node12.14.1-chrome85-ff81

WORKDIR /src/app
COPY . .

RUN npm ci 
RUN npm run cy:runlocal